#include <Arduino.h>
#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
  #include <SPIFFS.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
  #include <Hash.h>
  #include <FS.h>
#endif
#include <ESPAsyncWebServer.h>

AsyncWebServer server(80);

// REPLACE WITH YOUR NETWORK CREDENTIALS
const char* ssid = "GAGUTKA";
const char* password = "alamakota2";

const char* PARAM_INT = "inputInt";


// HTML web page
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv='refresh' content='5'/>
  <script>
    function submitMessage() {
      alert("Saved value to ESP SPIFFS");
      setTimeout(function(){ document.location.reload(false); }, 500);   
    }
  </script>
    
    <title>Oczyszczacz powietrza v1.0</title>
    <style>
      body { background-color: #c6e2ff; font-family: Arial, Helvetica, Sans-Serif; Color: #000088;font-size:45px }
    </style>
  </head>
  <body>
  <center>
    <img src="ventilation">
    <h5>Aktualne obroty:</h5>
    %inputInt%
    <h5>Ustaw obroty: (0-1300)</h5>
     <form action="/get"> 
    <input type="number " name="inputInt" style="font-size: 40 px; height:75px; width:400px">
    <input type="submit" value="Dawaj!" onclick="submitMessage()" style="font-size: 40 px; height:75px; width:200px">
  </form>

  </center>
  </body>
   
</html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");
}

String readFile(fs::FS &fs, const char * path){
  Serial.printf("Reading file: %s\r\n", path);
  File file = fs.open(path, "r");
  if(!file || file.isDirectory()){
    Serial.println("- empty file or failed to open file");
    return String();
  }
  Serial.println("- read from file:");
  String fileContent;
  while(file.available()){
    fileContent+=String((char)file.read());
  }
  file.close();
  Serial.println(fileContent);
  return fileContent;
}

void writeFile(fs::FS &fs, const char * path, const char * message){
  Serial.printf("Writing file: %s\r\n", path);
  File file = fs.open(path, "w");
  if(!file){
    Serial.println("- failed to open file for writing");
    return;
  }
  if(file.print(message)){
    Serial.println("- file written");
  } else {
    Serial.println("- write failed");
  }
  file.close();
}

// Replaces placeholder with stored values
String processor(const String& var){
  //Serial.println(var);
  if(var == "inputInt"){
    return readFile(SPIFFS, "/inputInt.txt");
  }

  return String();
}

void setup() {
  Serial.begin(115200);
  analogWriteFreq(10000);
  // Initialize SPIFFS
  
    if(!SPIFFS.begin()){
      Serial.println("An Error has occurred while mounting SPIFFS");
      return;
    }


  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  if (WiFi.waitForConnectResult() != WL_CONNECTED) {
    Serial.println("WiFi Failed!");
    return;
  }
  Serial.println();
  Serial.print("IP Address: ");
  Serial.println(WiFi.localIP());

  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html, processor);
  });

  // Send a GET request to <ESP_IP>/get?inputString=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    // GET inputInt value on <ESP_IP>/get?inputInt=<inputMessage>
 if (request->hasParam(PARAM_INT)) {
      inputMessage = request->getParam(PARAM_INT)->value();
      writeFile(SPIFFS, "/inputInt.txt", inputMessage.c_str());
    }
    else {
      inputMessage = "No message sent";
    }
    Serial.println(inputMessage);
    //request->send(200, "text/text", inputMessage);
  });

  // Picture
  server.on("/ventilation", HTTP_GET, [](AsyncWebServerRequest *request){
  request->send(SPIFFS, "/ventilation.png", "image/png");
});
  
  
  server.onNotFound(notFound);
  server.begin();
}

void loop() {
  // To access your stored values on inputString, inputInt, inputFloat
  int Duty_cycle = (readFile(SPIFFS, "/inputInt.txt").toInt())/1.3;
  analogWrite(14, Duty_cycle);
  delay(1000);
      Serial.println();
}
